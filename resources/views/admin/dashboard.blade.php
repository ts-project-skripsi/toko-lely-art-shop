@extends('admin.layouts.app')

@section('title')
    Dashboard
@endsection

@section('content')
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-2">
          <div class="h4 fw-500">Dashboard</div>
        </div>
        <!--end breadcrumb-->
        <div class="row row-cols-1 row-cols-lg-2 row-cols-xxl-4">
          <div class="col">
            <div class="card radius-10">
              <div class="card-body">
                <div class="d-flex align-items-start gap-2">
                  <div>
                    <p class="mb-0 fs-6">Total Customer</p>
                  </div>
                  <div class="ms-auto widget-icon-small text-white bg-gradient-purple">
                    <ion-icon name="people-outline"></ion-icon>
                  </div>
                </div>
                <div class="d-flex align-items-center mt-3">
                  <div>
                    <h4 class="mb-0">{{ count($customer) }}</h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col">
            <div class="card radius-10">
              <div class="card-body">
                <div class="d-flex align-items-start gap-2">
                  <div>
                    <p class="mb-0 fs-6">Total Produk</p>
                  </div>
                  <div class="ms-auto widget-icon-small text-white bg-gradient-success">
                    <ion-icon name="layers"></ion-icon>
                  </div>
                </div>
                <div class="d-flex align-items-center mt-3">
                  <div>
                    <h4 class="mb-0">{{ $produk }}</h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col">
            <div class="card radius-10">
              <div class="card-body">
                <div class="d-flex align-items-start gap-2">
                  <div>
                    <p class="mb-0 fs-6">Menunggu pembayaran</p>
                  </div>
                  <div class="ms-auto widget-icon-small text-white bg-gradient-info">
                    <ion-icon name="alarm-outline"></ion-icon>
                  </div>
                </div>
                <div class="d-flex align-items-center mt-3">
                  <div>
                    <h4 class="mb-0">
                        {{ $order->where('status','menunggu_pembayaran')->count() }}
                    </h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col">
            <div class="card radius-10">
              <div class="card-body">
                <div class="d-flex align-items-start gap-2">
                  <div>
                    <p class="mb-0 fs-6">Pesanan Selesai</p>
                  </div>
                  <div class="ms-auto widget-icon-small text-white bg-gradient-danger">
                    <ion-icon name="checkmark-done-outline"></ion-icon>
                  </div>
                </div>
                <div class="d-flex align-items-center mt-3">
                  <div>
                    <h4 class="mb-0">
                        {{ $order->where('status','diterima')->count() }}
                    </h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--end row-->

        <div class="row row-cols-1 row-cols-lg-2">
          <div class="col">
            <div class="card radius-10 w-100">
              <div class="card-body">
                <div class="d-flex align-items-center mb-3">
                  <h6 class="mb-0"><span style="color: #ff8906">List Customer</span> </h6>
                </div>
                <div class="countries-list">
                    @foreach ($customer->take(6) as $data)
                    <div class="d-flex align-items-center gap-3 mb-3">
                        <div class="country-name flex-grow-1">
                        <h5 class="mb-0">{{ $data->nama_lengkap }}</h5>
                        <p class="mb-0 text-secondary">{{ $data->user->email }}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
              </div>
            </div>
          </div>

          <div class="col">
            <div class="card radius-10">
              <div class="card-body">
                <div class="d-flex justify-content-between align-content-center align-items-center mb-3">
                    <h6 class="mb-0">Grafik Pembelian</h6>
                    @php
                        $bulans = [
                          'Januari',
                          'Februari',
                          'Maret',
                          'April',
                          'Mei',
                          'Juni',
                          'Juli',
                          'Agustus',
                          'September',
                          'Oktober',
                          'November',
                          'Desember'
                        ]
                    @endphp
                    <select name="" id="select_month" class="form form-control form-control-sm w-50">
                      <option value="" selected>Semua</option>
                      @foreach ($bulans as $key => $bulan)
                        <option value="{{$key + 1}}" {{($key + 1) == $month ? 'selected': ''}}>{{$bulan}}</option>
                      @endforeach
                    </select>
                  </div>
                <div class="d-flex align-items-center gap-2 mb-3">
                  <h2 class="mb-0" id="total-transaksi-done">0</h2>
                </div>
                <div id="chart1"></div>
                {{-- <div class="mt-4">
                  <div class="d-flex align-items-center gap-3 mb-3">
                    <div class="widget-icon-small rounded bg-light-success text-success">
                      <ion-icon name="wallet-outline"></ion-icon>
                    </div>
                    <div class="flex-grow-1">
                      <h6 class="mb-0">$545.69</h6>
                      <p class="mb-0 text-secondary">Last Month Sales</p>
                    </div>
                    <div class="">
                      <p class="mb-0 text-success d-flex gap-1 align-items-center fw-500 fs-6"><i class='bx bx-up-arrow-alt'></i><span>35%</span></p>
                    </div>
                  </div>
                  <div class="d-flex align-items-center gap-3 mb-3">
                    <div class="widget-icon-small rounded bg-light-tiffany text-tiffany">
                      <ion-icon name="flag-outline"></ion-icon>
                    </div>
                    <div class="flex-grow-1">
                      <h6 class="mb-0">$956.34</h6>
                      <p class="mb-0 text-secondary">Last Month Sales</p>
                    </div>
                    <div class="">
                      <p class="mb-0 text-danger d-flex gap-1 align-items-center fw-500 fs-6"><i class='bx bx-up-arrow-alt'></i><span>45%</span></p>
                    </div>
                  </div>
                  <div class="d-flex align-items-center gap-3">
                    <div class="widget-icon-small rounded bg-light-danger text-danger">
                      <ion-icon name="school-outline"></ion-icon>
                    </div>
                    <div class="flex-grow-1">
                      <h6 class="mb-0">$6956.48</h6>
                      <p class="mb-0 text-secondary">Last Year Sales</p>
                    </div>
                    <div class="">
                      <p class="mb-0 text-success d-flex gap-1 align-items-center fw-500 fs-6"><i class='bx bx-up-arrow-alt'></i><span>66%</span></p>
                    </div>
                  </div>
                </div> --}}
              </div>
            </div>
          </div>

          <div class="col-lg-12">
            <div class="card radius-10 w-100">
              <div class="card-body">
                <div class="d-flex align-items-center">
                  <h6 class="mb-0" style="color: #ff8906">Pembelian Terakhir</h6>
                </div>
                <div class="table-responsive mt-2">
                  <table class="table align-middle mb-0">
                    <thead class="table-light">
                      <tr>
                        <th>No</th>
                        <th>Invoice</th>
                        <th>Customer</th>
                        <th>Alamat</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th class="text-center">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                        @php
                        $index = 1;
                     @endphp
                     @foreach ($order as $orders)
                     <tr>
                         <td>{{ $index++ }}</td>
                         <td>{{ $orders->invoice }}</td>
                         <td>{{ ucfirst($orders->customers->nama_lengkap) }}</td>
                         <td>{{ $orders->alamat }}</td>
                         <td>Rp {{ number_format($orders->total,0,',','.') }}</td>
                         <td>
                             @if ($orders->status == 'pending')
                                 <span class="bg-red-100 text-red-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-red-900 dark:text-red-300">
                                     Konfirmasi Pesanan
                                 </span>
                             @elseif($orders->status == 'menunggu_pembayaran')
                                 <span class="bg-yellow-100 text-yellow-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-yellow-900 dark:text-yellow-300">
                                     Menunggu Pembayaran
                                 </span>
                             @elseif($orders->status == 'menunggu_persetujuan')
                                 <span class="bg-yellow-100 text-yellow-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-yellow-900 dark:text-yellow-300">
                                     Konfirmasi Pembayaran
                                 </span>
                             @elseif($orders->status == 'terbayar')
                                 <span class="bg-green-100 text-green-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-green-900 dark:text-green-300">
                                     Sudah terbayar
                                 </span>
                             @elseif($orders->status == 'pengiriman')
                                 <span class="bg-blue-100 text-blue-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-blue-900 dark:text-blue-300">
                                     Sedang Pengiriman
                                 </span>
                             @elseif($orders->status == 'diterima')
                                 <span class="bg-green-100 text-green-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-green-900 dark:text-green-300">
                                     Pesanan Diterima
                                 </span>
                             @elseif($orders->status == 'dibatalkan')
                                 <span class="bg-red-100 text-red-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-red-900 dark:text-red-300">
                                     Dibatalkan
                                 </span>
                             @endif
                         </td>
                         <td>
                             <div class="d-flex justify-content-center align-items-center">
                                 <a class="btn btn-info" href="{{ route('order.show',$orders->id) }}">Detail </a>
                             </div>
                         </td>
                     </tr>
                     @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
@endsection


@section('js-content')
<script>
  $(document).ready(function () {
      $('#select_month').on('change',function (e) {
         window.location.href = "{{ route('dashboard.index') }}?month=" + e.target.value;
      });
  });
</script>
@endsection



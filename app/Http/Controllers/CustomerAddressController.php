<?php

namespace App\Http\Controllers;

use App\Models\customer_address;
use Illuminate\Http\Request;

class CustomerAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\customer_address  $customer_address
     * @return \Illuminate\Http\Response
     */
    public function show(customer_address $customer_address)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\customer_address  $customer_address
     * @return \Illuminate\Http\Response
     */
    public function edit(customer_address $customer_address)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\customer_address  $customer_address
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, customer_address $customer_address)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\customer_address  $customer_address
     * @return \Illuminate\Http\Response
     */
    public function destroy(customer_address $customer_address)
    {
        //
    }
}

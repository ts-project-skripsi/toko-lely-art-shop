<?php

namespace App\Http\Controllers;

use App\Models\produk_variants;
use Illuminate\Http\Request;

class ProdukVariantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\produk_variants  $produk_variants
     * @return \Illuminate\Http\Response
     */
    public function show(produk_variants $produk_variants)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\produk_variants  $produk_variants
     * @return \Illuminate\Http\Response
     */
    public function edit(produk_variants $produk_variants)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\produk_variants  $produk_variants
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, produk_variants $produk_variants)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\produk_variants  $produk_variants
     * @return \Illuminate\Http\Response
     */
    public function destroy(produk_variants $produk_variants)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\order_produks;
use Illuminate\Http\Request;

class OrderProduksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\order_produks  $order_produks
     * @return \Illuminate\Http\Response
     */
    public function show(order_produks $order_produks)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\order_produks  $order_produks
     * @return \Illuminate\Http\Response
     */
    public function edit(order_produks $order_produks)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\order_produks  $order_produks
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, order_produks $order_produks)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\order_produks  $order_produks
     * @return \Illuminate\Http\Response
     */
    public function destroy(order_produks $order_produks)
    {
        //
    }
}
